package model;

/**
 * Ensemble des directions possibles
 *
 * @author Medhi Louison et Valentin Perignon
 */
public enum CardinalPoint {
	EAST,
	NORTH_EAST,
	NORTH,
	NORTH_WEST,
	WEST,
	SOUTH_WEST,
	SOUTH,
	SOUTH_EAST
}

