package main;

import view.MainFrame;

/**
 * Classe principale
 *
 * @author Medhi Louison et Valentin Perignon
 */
public class Game {
	public static void main(String args[]) {
		// Lancement de la fenêtre principale
		MainFrame frame = new MainFrame();
	}
}
